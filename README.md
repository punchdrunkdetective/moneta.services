# Moneta Services 


## What is this repository for? 

moneta.services is expressjs app currently under development to provide data access to objects defined by moneta.juno.


## How do I get set up? 

npm install -g pm2 (for managing and testing the service live)
npm install -g heroku (for deploying the service)


## Contribution guidelines 

TBD


## Who do I talk to? 

Me! at punchdrunkdetective@gmail.com

