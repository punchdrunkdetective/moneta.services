var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var budgetsRouter = require('./routes/budgets');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false })); // form parsing turned off
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/budgets', budgetsRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// log out errors for debugging
app.use(function(err, req, res, next) {
  // TODO: Figure out a better way of logging. This can cause alot of noise.
  console.log(err.stack || 'no stack');
  console.log(req.body);
  next(err);
});

// error hander
app.use(function(err, req, res, next) {

  const status = err.status || 500;
  const message = err.message || 'unknown error';

  res.status(status);
  res.json({
    code: status,
    message: message
  });

});

// TODO: It would be nice for some errors to return json, others return a web page.
// error handler
/*
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
*/

module.exports = app;
