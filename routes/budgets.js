
const express = require('express');
const router = express.Router();

const { Budget } = require('moneta.juno');

router.post('/', function(req, res, next) {

  try {
    let b = new Budget(req.body);
    res.status(201).end();
  } catch (err) {
    err.status = 400;
    next(err);
  }

});

module.exports = router;
